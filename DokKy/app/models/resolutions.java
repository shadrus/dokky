package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 08.05.13
 * Time: 14:42
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;
import controllers.paramSearch;
import org.codehaus.jackson.annotate.*;
import com.avaje.ebean.*;

@Entity
public class resolutions extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer id;

    //id родительской резолюции пусто если резолюция первая
    public Integer pid;

    //ссылка на документ
    @ManyToOne
    @JoinColumn(name="docs_id")
    @JsonBackReference
    public docs docs_r;

    //текст резолюции
    public String text;

    //Краткий отчет
    public String res_report;

    //ссылка на руководителя
    @ManyToOne
    @JoinColumn(name="res_from",referencedColumnName="users_id")
    public users user_from_r;

    //ссылка на исполнителя
    @ManyToOne
    @JoinColumn(name="res_to",referencedColumnName="users_id")
    public users user_to_r;

    @Formats.DateTime(pattern="dd/MM/yyyy")
    public Date control_date;

    public static Finder<Integer,resolutions> find = new Finder<Integer,resolutions>(Integer.class, resolutions.class);

    //возвращаем количество записей для пагинации
    public static List<resolutions> all (){

        return find.orderBy("otdel_code, otdel_code desc").findList();
    }

}

