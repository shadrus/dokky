package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 23.05.13
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;
import controllers.paramSearch;

import com.avaje.ebean.*;

@Entity
public class system_email extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer system_id;


    @Constraints.Required
    public String system_smtp;

    @Constraints.Required
    public String system_login;

    @Constraints.Required
    public String system_password;

    @Constraints.Required

    public String system_address;


    public static Finder<Integer,system_email> find = new Finder<Integer,system_email>(Integer.class, system_email.class);

    //возвращаем количество записей для пагинации
    public static List<system_email> all (){

        return find.all();
    }



}

