package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 07.05.13
 * Time: 11:47
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;
import javax.persistence.criteria.Predicate;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;
import controllers.paramSearch;
import org.codehaus.jackson.annotate.*;

import com.avaje.ebean.*;
import models.utils.getPeriod;

@Entity
public class docs extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer docs_id;

    public String docs_number;

    public String docs_number_in;

    @Constraints.Required
    public String docs_subject;


   // @ManyToOne
   // @JoinColumn(name="contr_id",referencedColumnName="contr_id")
   // public contragents contragents_d;
    @Constraints.Required
    public String docs_contr;

    @Formats.DateTime(pattern="dd/MM/yyyy")
    @Constraints.Required
    public Date docs_date;

    public boolean docs_status;


    //тип документа 0 - входящие, 1 - исходящие, 2 - СЗ
    @Constraints.Required
    public Integer docs_type;

    public String docs_scan_id;

    public String docs_answer;

    @ManyToOne
    @JoinColumn(name="users_id",referencedColumnName="users_id")
    public users users_d;

    @OneToMany  (targetEntity = resolutions.class, mappedBy = "docs_r", cascade = CascadeType.ALL)
    @JsonManagedReference
    public List <resolutions> resolutions_d;

    public boolean docs_done;

    public static Finder<Integer,docs> find = new Finder<Integer,docs>(Integer.class, docs.class);


    //Выборка всех документов вообще
    public static List <docs> all (){
        return find.all();
    }


    public static List <docs> findCtrlDay (Integer days){

        Calendar needday = Calendar.getInstance();

        needday.add(Calendar.DAY_OF_YEAR, +days);
        List<docs> docs = find.where()
                          .eq("docs_status", true)
                          .le("resolutions_d.control_date", needday.getTime())
                          .findList();
        return docs;

    }

    //Ищем не было ли такого входящего ранее
    public static Boolean findPrevReg (String number, String contragent){
        Boolean result= false;
        List<docs> doc = find.where()
                .eq("docs_number_in", number)
                .eq("docs_contr", contragent)
                .findList();
        if (doc.size()>0){
            result = true;
        }
         return result;
    }

    //Ищем все входящие
    public static List<docs> findReportDocs (Integer docs_type){
        List<docs> doc = find.where()
                .eq("docs_type", docs_type)
                .findList();

        return doc;
    }
    //Ищем входящие начиная с doc_id_start
    public static List<docs> findReportDocs (Integer docs_type, String doc_id_start){
        Integer doc_start_id=Ebean.find(docs.class).where().eq("docs_number", doc_id_start).findUnique().docs_id;
        List<docs> doc = find.where()
                .eq("docs_type", docs_type)
                .ge("docs_id", doc_start_id)
                .findList();

        return doc;
    }

    //Ищем входящие начиная с doc_id_start  и заканчивая doc_id_end
    public static List<docs> findReportDocs (Integer docs_type, String doc_id_start, String doc_id_end){
        Integer doc_start_id=Ebean.find(docs.class).where().eq("docs_number", doc_id_start).findUnique().docs_id;
        Integer doc_end_id=Ebean.find(docs.class).where().eq("docs_number", doc_id_end).findUnique().docs_id;

        List<docs> doc = find.where()
                .eq("docs_type", docs_type)
                .ge("docs_id", doc_start_id)
                .le("docs_id", doc_end_id)
                .findList();

        return doc;
    }

    //DDDВыборка всех документов указанного типа и года
    public static List <docs> getQuery (int type, Date docdate, int users_id){
        getPeriod period = new getPeriod(docdate);
        users user = Ebean.find(users.class).setId(users_id).findUnique();
        //показываем все документы, если читаем делопроизводитель или администрация
        //TODO администрация должна иметь фиксированный id
        if (user.status_u.statuses_id==2 || user.dolgn_u.otdel_d.otdel_id==7){
        return find
                .fetch("users_d", "users_id, users_fio")
                .where()
                .between("docs_date", period.getStart(), period.getEnd())
                .eq("docs_type", type)
                .setOrderBy("docs_id desc, docs_date desc")
                .findList();
        }  else {
            return find
                    .fetch("users_d", "users_id, users_fio")
                    .where()
                    .between("docs_date", period.getStart(), period.getEnd())
                    .eq("docs_type", type)
                    .or(com.avaje.ebean.Expr.or(
                       com.avaje.ebean.Expr.eq("users_d.dolgn_u.otdel_d.otdel_id", user.dolgn_u.otdel_d.otdel_id),
                       com.avaje.ebean.Expr.eq("resolutions_d.user_to_r.dolgn_u.otdel_d.otdel_id",  user.dolgn_u.otdel_d.otdel_id)
                    ) , com.avaje.ebean.Expr.eq("resolutions_d.user_from_r.dolgn_u.otdel_d.otdel_id",  user.dolgn_u.otdel_d.otdel_id)

                    )
                    //.eq("users_d.dolgn_u.otdel_d.otdel_id", user.dolgn_u.otdel_d.otdel_id )
                    .setOrderBy("docs_id desc, docs_date desc")
                    .findList();
        }
    }

    //Получение года самого первого документа нужного типа для формирования списка архива
    public static Integer getFirstDate (int type){
        Calendar year = Calendar.getInstance();
        Date firstDate;
        try{
                 firstDate = find
                .select("docs_date")
                .order("docs_date asc")
                .setMaxRows(1)
                .where()
                .eq("docs_type", type)
                .findUnique().docs_date;
            year.setTime(firstDate);

        } catch (NullPointerException ex){
            firstDate=year.getTime();

        }

        return year.get(Calendar.YEAR);
    }

    public static List <docs> byId (int id){
        return find.where()
                .eq("otdel_d.otdel_id", id)
                .findList();
    }

    //получаем количество документов в переданном году, для формирования очередного номера письма
    public static Integer getOutNumber (Date docdate, int docs_type){
        getPeriod period = new getPeriod(docdate);
        List <docs> lastDoc = find.where()
                .between("docs_date", period.getStart(), period.getEnd())
                .eq("docs_type", docs_type)
                .findList();
        //разбиваем номер последнего документа на составляющие, разделенные "/"
        Integer lastId=0;
        if (lastDoc.size()>0){
               if (docs_type==0){
                   lastId = Integer.parseInt(lastDoc.get(lastDoc.size()-1).docs_number);

               }
               else if (docs_type==1){
               String [] tmpstr = lastDoc.get(lastDoc.size()-1).docs_number.split("/");
               //возвращаем номер после дроби, он второй в массиве
                   lastId = Integer.parseInt(tmpstr[1]);
                   return  lastId;
               }

        }

        return  lastId;
    }




}
