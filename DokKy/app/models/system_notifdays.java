package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 10.06.13
 * Time: 11:58
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;



@Entity
public class system_notifdays extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer system_id;

    public Integer system_days;


    public static Finder<Integer,system_notifdays> find = new Finder<Integer,system_notifdays>(Integer.class, system_notifdays.class);


    public static List<system_notifdays> all (){

        return find.all();
    }



}

