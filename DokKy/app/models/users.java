package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 30.04.13
 * Time: 11:08
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;
import controllers.paramSearch;

import com.avaje.ebean.*;

@Entity
public class users extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer users_id;


    @Constraints.Required
    public String users_login;


    public String users_password;


    @ManyToOne
    @JoinColumn(name="statuses_id",referencedColumnName="statuses_id")
    public statuses status_u;

    @ManyToOne
    @JoinColumn(name="dolgn_id",referencedColumnName="dolgn_id")
    public dolgn dolgn_u;

    @Constraints.Email
    public String users_email;

    public String users_fio;

    public String users_phone1;

    public String users_phone2;

    public String users_phone3;

    public String users_about;

    public String users_photo;

    public boolean users_isactive;

    public static Finder<Integer,users> find = new Finder<Integer,users>(Integer.class, users.class);

    //возвращаем количество записей для пагинации
    public static List<users> all (){

        return find.orderBy("users_fio, users_fio desc").where().eq("users_isactive", true).findList();
    }

    public static users findByName (String name){

        return find.where().eq("users_login", name).findUnique();
    }

    public static String getCode (int id){

        return find.byId(id).dolgn_u.otdel_d.otdel_code;
    }

}

