package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 25.04.13
 * Time: 8:27
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;
import controllers.paramSearch;

import com.avaje.ebean.*;

@Entity
public class dolgn extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer dolgn_id;

    @Constraints.Required
    public String dolgn_name;

    @ManyToOne
    @JoinColumn(name="otdel_id",referencedColumnName="otdel_id")
    public otdel otdel_d;


    public static Finder<Integer,dolgn> find = new Finder<Integer,dolgn>(Integer.class, dolgn.class);

    public static List <dolgn> all (){
        return find.all();
    }

    public static List <dolgn> byId (int id){
        return find.where()
                .eq("otdel_d.otdel_id", id)
                .findList();
    }

    public static Page<dolgn> page(int page) {
        return
                find.where()
                        .findPagingList(20)
                        .getPage(page);
    }

    public static Page<dolgn> pageSearch(int page, paramSearch newsearch) {
        return
                find.where()
                        .ilike("dolgn_name", "%"+newsearch.param+"%")
                        .findPagingList(20)
                        .getPage(page);
    }


}
