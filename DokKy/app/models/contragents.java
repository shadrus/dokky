package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 07.05.13
 * Time: 11:53
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;
import controllers.paramSearch;

import com.avaje.ebean.*;

@Entity
public class contragents extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer contr_id;

    @Constraints.Required
    public String contr_name;

    public String contr_adress;

    public String contr_tel;

    public String contr_bossfio;

    public String contr_bossdolg;



    public static Finder<Integer, contragents> find = new Finder<Integer,contragents>(Integer.class, contragents.class);

    public static List <contragents> all (){
        return find.all();
    }

    public static List <contragents> byId (int id){
        return find.where()
                .eq("otdel_d.otdel_id", id)
                .findList();
    }



    public static Page<contragents> page(int page) {
        return
                find.where()
                        .findPagingList(20)
                        .getPage(page);
    }

    public static Page<contragents> pageSearch(int page, paramSearch newsearch) {
        return
                find.where()
                        .ilike("dolgn_name", "%"+newsearch.param+"%")
                        .findPagingList(20)
                        .getPage(page);
    }


}
