package models.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 21.05.13
 * Time: 8:39
 * To change this template use File | Settings | File Templates.
 */
public class getPeriod {
    private Date start;
    private Date end;

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    public getPeriod (Date docdate){

        Calendar now = Calendar.getInstance();
        now.setTime(docdate);
        int year=now.get(Calendar.YEAR);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.WEEK_OF_YEAR, 1);
        cal.set(Calendar.DAY_OF_WEEK, 1);
        start = cal.getTime();

        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, 11); // 11 = december
        cal.set(Calendar.DAY_OF_MONTH, 31); // new years eve
        end = cal.getTime();
    }


}
