package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 30.04.13
 * Time: 11:17
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;
import controllers.paramSearch;

import com.avaje.ebean.*;

@Entity
public class statuses extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer statuses_id;


    @Constraints.Required
    public String statuses_name;


    public static Finder<Integer,statuses> find = new Finder<Integer,statuses>(Integer.class, statuses.class);

    //возвращаем количество записей для пагинации
    public static List<statuses> all (){

        return find.all();
    }

    //возращаем записи запрошенной страницы
    public static List<statuses> page(int page) {
        return
                find.where()
                        .findPagingList(20)
                        .getPage(page)
                        .getList();
    }

    public static Page<statuses> pageSearch(int page, paramSearch newsearch) {
        return
                find.where()
                        .ilike("statuses_name", "%"+newsearch.param+"%")
                        .findPagingList(20)
                        .getPage(page);
    }


}

