package models;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 24.04.13
 * Time: 12:11
 * To change this template use File | Settings | File Templates.
 */
import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;
import play.db.ebean.Model.Finder;
import controllers.paramSearch;

import com.avaje.ebean.*;

@Entity
public class otdel extends Model {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Integer otdel_id;


    @Constraints.Required
    public String otdel_code;

    @Constraints.Required
    public String otdel_name;


    public static Finder<Integer,otdel> find = new Finder<Integer,otdel>(Integer.class, otdel.class);

    //возвращаем количество записей для пагинации
    public static List<otdel> all (){

        return find.orderBy("otdel_code, otdel_code desc").findList();
    }

    //возращаем записи запрошенной страницы
    public static List<otdel> page(int page) {
        return
                find.where()
                        .findPagingList(20)
                        .getPage(page)
                        .getList();
    }

    public static Page<otdel> pageSearch(int page, paramSearch newsearch) {
        return
                find.where()
                        .ilike("otdel_name", "%"+newsearch.param+"%")
                        .findPagingList(20)
                        .getPage(page);
    }


}
