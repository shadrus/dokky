package controllers;

import com.avaje.ebean.Ebean;
import models.docs;
import models.resolutions;
import models.users;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 01.08.13
 * Time: 9:33
 * Класс предназначен для получения списка users_id, которым отписан документ
 */
public class GetResolutionsByDocID {
    private Integer docId;

    public GetResolutionsByDocID (Integer docId){
        this.docId=docId;

    }
    public List<Integer> get(){
       List<Integer>usersList = new ArrayList();
       List<resolutions> resolList=Ebean.find(docs.class).setId(docId).findUnique().resolutions_d;

       if (resolList.get(0).user_to_r!=null){
           for (int i=0; i<resolList.size();i++){
               usersList.add(resolList.get(i).user_to_r.users_id);
       }
       }

       return  usersList;
    }

}
