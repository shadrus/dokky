package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.text.json.JsonContext;
import com.avaje.ebean.text.json.JsonWriteOptions;
import models.*;
import org.imgscalr.Scalr;
import play.data.DynamicForm;

import static play.data.Form.*;

import play.data.Form;
import play.libs.Akka;
import play.libs.F.Promise;
import play.libs.F.Function;

import play.mvc.*;
import play.libs.Json;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;



import play.api.Logger;

public class Application extends Controller {
    final static public String curIndex = "index7.html";
    final static private play.Logger.ALogger logger = play.Logger.of("application");
    final static private String falseAuth = "{isAuth: false}";
    final static private String trueAuth = "isAuth: true";
    static private controllers.Status userstatus;

    public static Result index() {
        if (session("id")!=null){
            return redirect(routes.Assets.at(curIndex+"#/inbox"));

        } else{
        return redirect(routes.Assets.at(curIndex+"#/login"));
        }
    }

    public static Result getsession() {
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        if (session("connected") == null) {
            if (callbackName != null) {
                // JSONP wrapping:
                callbackName = callbackName +
                        "([" + falseAuth + "])";

            }
        } else {
            if (callbackName != null)

            {
                users user_status = Ebean.find(users.class).setId(session("id")).findUnique();
                userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
                // JSONP wrapping:
                callbackName = callbackName +
                        "([{" + trueAuth + ", user_status: " + user_status.status_u.statuses_id + ", id:"+user_status.users_id+ "}])";

            }
        }

        return ok(callbackName);
    }

    public static Result login() {

        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        String login = DynamicForm.form().bindFromRequest().get("login");
        String password = DynamicForm.form().bindFromRequest().get("password");
        ToSha sha = new ToSha();
        password = sha.doSHA(password);
        users user = new users();
        user = users.findByName(login);
        Integer result;
        if (user != null) {

            if (user.users_password.equals(password)) {
                session("connected", login);
                session("id", String.valueOf(user.users_id));
                logger.info(user.users_login + " entered");
                result = user.status_u.statuses_id;
                userstatus = new UserStatus(Integer.parseInt(session("id"))).get();

            } else {
                result = 0;
            }
            if (callbackName != null) {
                // JSONP wrapping:
                callbackName = callbackName +
                        "({result:" + result +", id:"+user.users_id+"})";

            }
        } else {
            result = 0;
            if (callbackName != null) {
                // JSONP wrapping:
                callbackName = callbackName +
                        "({result:" + result +"})";

            }
        }


        return ok(callbackName);
    }

    public static Result logout() {

        String callbackName = DynamicForm.form().bindFromRequest().get("callback");

        users user = Ebean.find(users.class).setId(session("id")).findUnique();
        session().remove("connected");
        session().remove("id");

        if (callbackName != null) {
            logger.info(user.users_login + " exit from system");
            // JSONP wrapping:
            callbackName = callbackName +
                    "({result: 1})";

        }
        return ok(callbackName);
    }

    //выгрузка журнала корреспонденции
    public static Result getJournalRep() {

        Integer doc_type = Integer.parseInt(DynamicForm.form().bindFromRequest().get("type_id"));
        String doc_start_number = DynamicForm.form().bindFromRequest().get("doc_start_number");
        String doc_end_number = DynamicForm.form().bindFromRequest().get("doc_end_number");

        Reports report = new Reports();
        if ((doc_end_number.isEmpty())&&(doc_start_number.isEmpty())){
            List<docs> doc = docs.findReportDocs(doc_type);
            report.getJournal(doc);
        } else if ((doc_end_number.isEmpty())&&(!doc_start_number.isEmpty())){
            List<docs> doc = docs.findReportDocs(doc_type, doc_start_number);
            report.getJournal(doc);
        } else if ((!doc_end_number.isEmpty())&&(!doc_start_number.isEmpty())){
            List<docs> doc = docs.findReportDocs(doc_type, doc_start_number, doc_end_number);
            report.getJournal(doc);
        }

        response().setContentType("application/vnd.ms-excel");
        File file = new File("../DokKy/public/reports/report.xls");
        response().setHeader("Content-Disposition", "attachment; filename*=report.xls");
        response().setHeader("Content-Length", String.valueOf(file.length()));
        try {
            return ok(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return ok ("File not found");
        }
    }

    public static Result getOtdels() {

        String callbackName = DynamicForm.form().bindFromRequest().get("callback");

        if (session("connected") != null) {
            otdel otdels = new otdel();

            if (callbackName != null) {
                // JSONP wrapping:
                callbackName = callbackName +
                        "(" + Json.toJson(otdels.all()) + ")";

            }
        }
        Reports report = new Reports();
        List<docs> doc = docs.findReportDocs(0);
        report.getJournal(doc);
        return ok(callbackName);
    }

    public static Result addOtdel() {
        //авторизация протестирована
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        users user = Ebean.find(users.class).setId(session("id")).findUnique();
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        if (callbackName != null && userstatus == controllers.Status.ADMIN) {
            // разрешаю добавлять отдел только админу
            Form<otdel> addOtdel = form(otdel.class);
            otdel newotdel = addOtdel.bindFromRequest().get();
            newotdel.save();
            logger.info(user.users_login + " has added department with id " + newotdel.otdel_id);
            callbackName = callbackName +
                    "(" + Json.toJson(newotdel) + ")";
            return ok(callbackName);
        } else {
            return ok("{'result': 'Error'}");
        }

    }


    //устанавливаем статус письма как исполнено
    public static Result setIsp() {

        users user = Ebean.find(users.class).setId(session("id")).findUnique();
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        docs doc = Ebean.find(docs.class).setId(Integer.parseInt(DynamicForm.form().bindFromRequest().get("docs_id"))).findUnique();
        String report = DynamicForm.form().bindFromRequest().get("res_report");
        //исполнение может проставить или тот кому документ отписан или канцелярия, краткий отчет обязателен
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        boolean done=true;
        //проверяем является ли текущий пользователь один из исполнителей по письму
        GetRule getRule = new GetRule(doc.docs_id, Integer.parseInt(session("id")));
        boolean hasRule=getRule.get();

            if (callbackName != null && report != null && (hasRule==true || userstatus == controllers.Status.MANAGER)) {
            //перебираю все резолюции в посках той, что отписана текущему пользователю
            for (int i=0; i<doc.resolutions_d.size(); i++){
                //если нахожу, то добавляю его отчет исполнителя
                if (doc.resolutions_d.get(i).user_to_r.users_id==Integer.parseInt(session("id"))){
                    doc.resolutions_d.get(i).res_report = report;
                }
                //нужно выяснить от всех ли исполнителей проставлен отчет, если да, то письмо считается исполненным
                if (doc.resolutions_d.get(i).res_report==null){
                      done=false;
                }
            }
            //нужно выяснить от всех ли исполнителей проставлен отчет, если да, то письмо считается исполненным
            if (done==true){
            doc.docs_done = true;
            } else{
                doc.docs_done = false;
            }
            doc.save();
            // JSONP wrapping:
            callbackName = callbackName +
                    "(" + Json.toJson(doc.resolutions_d) + ")";
            return ok(callbackName);

        } else {
            return ok("Error");
        }
    }

    public static Result setControlDate() {
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        if (callbackName != null && userstatus == controllers.Status.MANAGER) {
            // разрешаю добавлять контрольную дату только делопроизводителю
            Integer id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("docs_id"));
            docs doc = Ebean.find(docs.class).setId(id).findUnique();
            Date date;
            try {
                date = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(DynamicForm.form().bindFromRequest().get("ctrl_date"));
                doc.resolutions_d.get(0).control_date = date;
                doc.docs_status = true;
                doc.save();
            } catch (ParseException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            if (callbackName != null) {
                // JSONP wrapping:
                callbackName = callbackName +
                        "(" + Json.toJson("ok") + ")";
                return ok(callbackName);
            } else {
                return ok("Error");
            }

        } else {
            return ok("Error");
        }
    }

    public static Result removeControlDate() {
        users user = Ebean.find(users.class).setId(session("id")).findUnique();
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        if (callbackName != null && userstatus == controllers.Status.MANAGER) {
            // разрешаю удалять контрольную дату только делопроизводителю
            Integer id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("docs_id"));
            docs doc = Ebean.find(docs.class).setId(id).findUnique();
            doc.docs_status = false;
            doc.resolutions_d.get(0).control_date = null;
            try {
                doc.update();
            } catch (Exception ex) {
                logger.info(ex.toString());

            }
            logger.info(user.users_login + " has deleted control date for document number " + doc.docs_number);

            if (callbackName != null) {
                // JSONP wrapping:
                callbackName = callbackName +
                        "(" + Json.toJson(doc) + ")";
                return ok(callbackName);
            } else {
                return ok("Error");
            }

        } else {
            return ok("Error");
        }


    }

    //дбавляем или обновляем системное мыло
    public static Result addSystemEmail() {
        // разрешаю добавлять системный email только админу
        users user = Ebean.find(users.class).setId(session("id")).findUnique();
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        if (callbackName != null && userstatus == controllers.Status.ADMIN) {
            Form<system_email> addMail = form(system_email.class);
            system_email newmail = addMail.bindFromRequest().get();
            Integer oldmail = Ebean.find(system_email.class).findRowCount();
            if (oldmail == 0) {
                newmail.save();
            } else {
                system_email email = Ebean.find(system_email.class).setId(1).findUnique();
                email.system_address = newmail.system_address;
                email.system_login = newmail.system_login;
                email.system_password = newmail.system_password;
                email.system_smtp = newmail.system_smtp;
                email.update();
                logger.info(user.users_login + " has changed system email to " + email.system_address);

            }
            JsonContext jsonContext = Ebean.createJsonContext();
            JsonWriteOptions options = new JsonWriteOptions();
            options.setRootPathProperties("system_address");
            return ok(jsonContext.toJsonString(newmail, false, options, callbackName));
        }

        return ok("Error");
    }

    public static Result getSystemEmail() {
        // разрешаю получить системный email только админу
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        List<system_email> email = Ebean.find(system_email.class).setId(1).findList();
        if (callbackName != null) {
            JsonContext jsonContext = Ebean.createJsonContext();
            JsonWriteOptions options = new JsonWriteOptions();
            options.setRootPathProperties("system_smtp, system_login, system_address");
            return ok(jsonContext.toJsonString(email, false, options, callbackName));
        } else {
            return ok(callbackName);
        }


    }

    public static Result getDolgn() {

        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        dolgn dolgnes = new dolgn();
        if (callbackName != null) {
            // JSONP wrapping:
            callbackName = callbackName +
                    "(" + Json.toJson(dolgnes.all()) + ")";
        }
        return ok(callbackName);
    }

    public static Result getDolgnId() {

        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        Integer id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("otdel_id"));

        dolgn dolgnes = new dolgn();
        if (callbackName != null) {
            // JSONP wrapping:
            callbackName = callbackName +
                    "(" + Json.toJson(dolgnes.byId(id)) + ")";
        }
        return ok(callbackName);
    }

    public static Result addDolgn() {
        users user = Ebean.find(users.class).setId(session("id")).findUnique();
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        if (callbackName != null && userstatus == controllers.Status.ADMIN) {
            Form<dolgn> addDolgn = form(dolgn.class);
            dolgn newdolgn = addDolgn.bindFromRequest().get();
            newdolgn.save();
            otdel ot = new otdel();
            ot = Ebean.find(otdel.class).setId(newdolgn.otdel_d.otdel_id).findUnique();
            newdolgn.otdel_d.otdel_code = ot.otdel_code;
            newdolgn.otdel_d.otdel_name = ot.otdel_name;
            JsonContext jsonContext = Ebean.createJsonContext();
            JsonWriteOptions options = new JsonWriteOptions();
            options.setRootPathProperties("dolgn_id, dolgn_name, otdel_d");
            options.setPathProperties("otdel_d", "otdel_id, otdel_name, otdel_code");
            logger.info(user.users_login + " has added dolgnost with id " + newdolgn.dolgn_id);
            return ok(jsonContext.toJsonString(newdolgn, false, options, callbackName));

        } else {
            return ok("Error");
        }
    }

    //руководитель добавляет резолюцию самостоятельно
    public static Result addResol() {
        List<users> usersList = new ArrayList();

        String test = DynamicForm.form().bindFromRequest().get("otvet");
        List<String> elephantList = Arrays.asList(test.split(","));
        for (int i = 0; i < elephantList.size(); i++) {
            usersList.add(i, Ebean.find(users.class).setId(Integer.parseInt(elephantList.get(i))).findUnique());
        }
        users userFrom;
        String resolFrom = DynamicForm.form().bindFromRequest().get("resol_from");
        if (resolFrom.equals("null")){
            userFrom = Ebean.find(users.class).setId(session("id")).findUnique();
        } else{
            userFrom = Ebean.find(users.class).setId(DynamicForm.form().bindFromRequest().get("resol_from")).findUnique();
        }
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        if (callbackName != null && (userstatus == controllers.Status.BOSS || userstatus == controllers.Status.MANAGER)) {
            Integer doc_id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("docs_id"));
            docs doc = Ebean.find(docs.class).setId(doc_id).findUnique();
            //удаляем предыдущие резолюции и потом сохраняем новые
            for (int i = 0; i < doc.resolutions_d.size(); i++) {
                doc.resolutions_d.get(i).delete();
            }
                for (int i = 0; i < usersList.size(); i++) {
                resolutions res=new resolutions();
                String text = DynamicForm.form().bindFromRequest().get("text");
                res.docs_r = doc;
                res.user_from_r = userFrom;
                res.user_to_r = usersList.get(i);

                if (!text.equals("undefined")) {
                    res.text = text;
                }
                try {
                    Date control = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(DynamicForm.form().bindFromRequest().get("control_date"));
                    res.control_date = control;
                    doc.docs_status = true;
                } catch (ParseException e) {
                }

                res.save();
                sendEmailNotif email = new sendEmailNotif();
                email.send(usersList.get(i).users_email, res.docs_r.docs_number, res.docs_r.docs_subject, usersList.get(i).users_fio, res.text, res.docs_r.docs_contr, res.docs_r.docs_id, res.control_date, 0);
            }

            doc.docs_done = false;
            doc.update();


            JsonContext jsonContext = Ebean.createJsonContext();
            JsonWriteOptions options = new JsonWriteOptions();
            options.setRootPathProperties("docs_id");
            return ok(jsonContext.toJsonString(doc, false, options, callbackName));

        } else {
            return ok("Error");
        }
    }

    public static Result delDolgn() {
        users user = Ebean.find(users.class).setId(session("id")).findUnique();
        if (session("connected") != null) {
            // разрешаю удалить должность только админу
            userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
            if (userstatus == controllers.Status.ADMIN) {
                Integer id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("dolgn_id"));
                dolgn deldolgn = Ebean.find(dolgn.class).setId(id).findUnique();
                logger.info(user.users_login + " has deleted dolgnost with id " + deldolgn.dolgn_id + " called " + deldolgn.dolgn_name);
                deldolgn.delete();
                return ok("Ok");
            } else {
                return ok("Error");
            }
        } else {
            return ok("Error");
        }

    }

    public static Result editDolgn() {
        users user = Ebean.find(users.class).setId(session("id")).findUnique();
        if (session("connected") != null) {
            // разрешаю редактировать должность только админу
            userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
            if (userstatus == controllers.Status.ADMIN) {
                String callbackName = DynamicForm.form().bindFromRequest().get("callback");
                Integer id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("dolgn_id"));
                String name = DynamicForm.form().bindFromRequest().get("dolgn_name");
                dolgn editdolgn = Ebean.find(dolgn.class).setId(id).findUnique();
                editdolgn.dolgn_name = name;
                editdolgn.update();
                logger.info(user.users_login + " has edited dolgnost with id " + editdolgn.dolgn_id + " called " + editdolgn.dolgn_name);
                if (callbackName != null) {
                    // JSONP wrapping:
                    callbackName = callbackName +
                            "([{success: true}])";
                }
                return ok(callbackName);
            } else {
                return ok("Error");
            }
        } else {
            return ok("Error");
        }

    }

    public static Result editNotifSet() {
        users user = Ebean.find(users.class).setId(session("id")).findUnique();
        if (session("connected") != null) {
            // разрешаю редактировать должность только админу
            userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
            if (userstatus == controllers.Status.ADMIN) {
                String callbackName = DynamicForm.form().bindFromRequest().get("callback");
                Integer days = Integer.parseInt(DynamicForm.form().bindFromRequest().get("system_days"));
                system_notifdays curSet = Ebean.find(system_notifdays.class).setId(1).findUnique();
                curSet.system_days = days;
                curSet.update();
                logger.info(user.users_login + " has edited email notification period to " + days + " days");
                if (callbackName != null) {
                    // JSONP wrapping:
                    callbackName = callbackName +
                            "([{success: true}])";
                }
                return ok(callbackName);
            } else {
                return ok("Error");
            }
        } else {
            return ok("Error");
        }

    }

    public static Result getNotifSet() {
        if (session("connected") != null) {
            // разрешаю редактировать должность только админу
            userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
            if (userstatus == controllers.Status.ADMIN) {
                String callbackName = DynamicForm.form().bindFromRequest().get("callback");
                system_notifdays curSet = Ebean.find(system_notifdays.class).setId(1).findUnique();
                if (callbackName != null) {
                    // JSONP wrapping:
                    callbackName = callbackName +
                            "(" + Json.toJson(curSet) + ")";
                }
                return ok(callbackName);
            } else {
                return ok("Error");
            }
        } else {
            return ok("Error");
        }

    }

    public static Result addUser() {
        //Из-за получения данных напрямую POST, а не через JSONP, приходится получать сессию через Context

        users user = Ebean.find(users.class).setId(ctx().session().get("id")).findUnique();
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        if (ctx().session().get("connected") != null && userstatus == controllers.Status.ADMIN) {
            Http.MultipartFormData body = request().body().asMultipartFormData();

            String hashfilename = null;
            BufferedImage in = null;
            String ext = null;
            File outputfile = null;
            if (body.getFile("photo") != null) {
                Http.MultipartFormData.FilePart letter = body.getFile("photo");

                String fileName = letter.getFilename();
                int dotPos = fileName.lastIndexOf(".") + 1;

                ext = fileName.substring(dotPos);
                if (ext.equals("jpeg") || ext.equals("jpg") || ext.equals("png")) {

                    File file = letter.getFile();

                    try {
                        in = ImageIO.read(file);
                        in = Scalr.resize(in, Scalr.Method.SPEED, Scalr.Mode.FIT_TO_WIDTH, 400, 400, Scalr.OP_ANTIALIAS);
                        hashfilename = UUID.randomUUID().toString() + "." + ext;
                        outputfile = new File("../DokKy/public/photos/" + hashfilename);


                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }


                }

            }
            Form<users> addUser = form(users.class);
            users newuser = addUser.bindFromRequest().get();
            newuser.users_photo = hashfilename;
            ToSha sha = new ToSha();
            newuser.users_password = sha.doSHA(newuser.users_password);
            newuser.users_isactive = true;

            newuser.save();
            //Сохраняем файл после сохранения в БД информации
            if (outputfile != null) {
                try {
                    ImageIO.write(in, ext, outputfile);
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }

        return redirect("/assets/"+curIndex+"#/users");

    }

    public static Result editOtdel() {

        users user = Ebean.find(users.class).setId(session("id")).findUnique();
        // разрешаю редактировать должность только админу
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        if (session("connected") != null && userstatus == controllers.Status.ADMIN) {
            // разрешаю редактировать должность только админу
            String callbackName = DynamicForm.form().bindFromRequest().get("callback");
            Integer id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("otdel_id"));
            String name = DynamicForm.form().bindFromRequest().get("otdel_name");
            String code = DynamicForm.form().bindFromRequest().get("otdel_code");
            otdel editotdel = Ebean.find(otdel.class).setId(id).findUnique();
            editotdel.otdel_name = name;
            editotdel.otdel_code = code;
            editotdel.update();
            logger.info(user.users_login + " has edited department with id " + editotdel.otdel_id + " called " + editotdel.otdel_name);

            if (callbackName != null) {
                // JSONP wrapping:
                callbackName = callbackName +
                        "([{success: true}])";
            }
            return ok(callbackName);
        } else {
            return ok("Error");
        }


    }

    public static Result editUser() {

        users user = Ebean.find(users.class).setId(ctx().session().get("id")).findUnique();
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        if (ctx().session().get("connected") != null && userstatus == controllers.Status.ADMIN) {
            Http.MultipartFormData body = request().body().asMultipartFormData();
            Integer id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("users_id"));
            users edituser = Ebean.find(users.class).setId(id).findUnique();
            String oldpassword = edituser.users_password;
            Form<users> editUser = form(users.class);
            edituser = editUser.bindFromRequest().get();

            String hashfilename;
            BufferedImage in = null;
            String ext = null;
            File outputfile = null;
            if (body.getFile("photo") != null) {
                Http.MultipartFormData.FilePart letter = body.getFile("photo");

                String fileName = letter.getFilename();
                int dotPos = fileName.lastIndexOf(".") + 1;

                ext = fileName.substring(dotPos);
                if (ext.equals("jpeg") || ext.equals("jpg") || ext.equals("png")) {

                    File file = letter.getFile();

                    try {
                        in = ImageIO.read(file);
                        in = Scalr.resize(in, Scalr.Method.SPEED, Scalr.Mode.FIT_TO_WIDTH, 400, 400, Scalr.OP_ANTIALIAS);
                        //TODO получить номер файла
                        hashfilename = UUID.randomUUID().toString() + "." + ext;
                        outputfile = new File("../DokKy/public/photos/" + hashfilename);
                        edituser.users_photo = hashfilename;

                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
            edituser.users_isactive = true;
            ToSha sha = new ToSha();

            if (edituser.users_password.equals("")) {
                edituser.users_password = oldpassword;
            } else {
                edituser.users_password = sha.doSHA(edituser.users_password);

            }
            edituser.update();
            //Сохраняем файл после сохранения в БД информации
            if (outputfile != null) {
                try {
                    ImageIO.write(in, ext, outputfile);
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return redirect("/assets/"+curIndex+"#/users");


    }

    public static Result delUser() {
        //авторизация оттестирована
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        users user = Ebean.find(users.class).setId(session("id")).findUnique();
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        if (callbackName != null && userstatus == controllers.Status.ADMIN) {
            Integer id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("users_id"));
            users deluser = Ebean.find(users.class).setId(id).findUnique();
            deluser.users_isactive = false;
            logger.info(user.users_login + " has deleted user " + deluser.users_login);
            deluser.update();
            // JSONP wrapping:
            callbackName = callbackName +
                    "(" + Json.toJson("ok") + ")";
            return ok(callbackName);

        } else {
            return ok("Error");
        }

    }

    public static Result delDoc() {
        //авторизация оттестирована
        final String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        final Integer id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("docs_id"));
        final users user = Ebean.find(users.class).setId(ctx().session().get("id")).findUnique();


        Promise<String> promiseOfResult = Akka.future(
                new Callable<String>() {
                    public String call() {
                        return delDocAsync(callbackName, id, user);
                    }
                }
        );
        return async(
                promiseOfResult.map(
                        new Function<String, Result>() {
                            public Result apply(String i) {
                                return ok(i);
                            }
                        }
                )
        );


    }

    public static String delDocAsync(String callbackName, Integer id, users user) {

        userstatus = new UserStatus(user.users_id).get();
        if (callbackName != null && userstatus == controllers.Status.MANAGER) {
            docs deldoc = Ebean.find(docs.class).setId(id).findUnique();
            if (deldoc.docs_scan_id != null) {
                File file = new File("../DokKy/public/uploads/" + deldoc.docs_scan_id);
                file.delete();
            }
            logger.info(user.users_login + " has deleted document number" + deldoc.docs_number);
            deldoc.delete();
            // JSONP wrapping:
            callbackName = callbackName +
                    "({result: 'ok'})";
            return callbackName;
        } else {
            return callbackName;
        }

    }

    public static Result getUsers() {

        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        users users = new users();
        if (session("connected") != null) {
            if (callbackName != null)

            {
                // JSONP wrapping:
                callbackName = callbackName +
                        "(" + Json.toJson(users.all()) + ")";
            }
        }
        return ok(callbackName);
    }

    public static Result getUserCard() {

        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        Integer id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("users_id"));
        List<users> user = Ebean.find(users.class).setId(id).findList();
        if (user.get(0).users_photo == null) {
            user.get(0).users_photo = "no-user.png";
        }
        if (callbackName != null) {
            JsonContext jsonContext = Ebean.createJsonContext();
            JsonWriteOptions options = new JsonWriteOptions();
            options.setRootPathProperties("users_id, users_login, users_fio, users_email, dolgn_u, status_u, users_phone1, users_phone2, users_phone3, users_about, users_photo");
            options.setPathProperties("dolgn_u", "dolgn_id, dolgn_name, otdel_d");
            options.setPathProperties("status_u", "statuses_id");
            options.setPathProperties("dolgn_u.otdel_d", "otdel_id, otdel_name");
            return ok(jsonContext.toJsonString(user, false, options, callbackName));
        }
        return ok("Bad request");
    }

    public static Result getDocCard() {
        if (session("id")!=null){
        users user = Ebean.find(users.class).setId(session("id")).findUnique();
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        Integer doc_id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("docs_id"));
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        List <docs> doc = Ebean.find(docs.class).setId(doc_id).findList();

        GetRule getRule = new GetRule(doc_id, Integer.parseInt(session("id")));
        boolean hasRule=getRule.get();
        if ((callbackName != null) && ((hasRule==true)||(userstatus== controllers.Status.MANAGER || userstatus== controllers.Status.BOSS))   ){
        //    if (callbackName != null){
            JsonContext jsonContext = Ebean.createJsonContext();
            JsonWriteOptions options = new JsonWriteOptions();
            //options.setRootPathProperties("users_id, users_fio, users_email, dolgn_u, users_phone1, users_phone2, users_phone3, users_about, users_photo");
            options.setPathProperties("users_d", "users_fio, users_id");
            options.setPathProperties("resolutions_d", "text, control_date, res_report, user_to_r, user_from_r, pid");
            options.setPathProperties("resolutions_d.user_to_r", "users_id, users_fio");
            options.setPathProperties("resolutions_d.user_from_r", "users_id, users_fio");

            return ok(jsonContext.toJsonString(doc, false, options, callbackName));
        } else{
            logger.error(user.users_login+" has tried to get wrong access");
            return redirect("/assets/"+curIndex+"#/users");
        }
        }
        logger.error("Somebody has tried to get wrong access");
        return redirect("/assets/"+curIndex+"#/users");
    }


    public static Result getStatuses() {

        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        statuses status = new statuses();
        if (callbackName != null) {
            // JSONP wrapping:
            callbackName = callbackName +
                    "(" + Json.toJson(status.all()) + ")";
        }
        return ok(callbackName);
    }

    public static Result getFirstDate() {
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        Integer type = Integer.parseInt(DynamicForm.form().bindFromRequest().get("type"));
        docs doc = new docs();
        if (callbackName != null) {
            // JSONP wrapping:
            callbackName = callbackName +
                    "({firstdate: " + doc.getFirstDate(type).toString() + "})";
        }
        return ok(callbackName);
    }

    public static Result getDocs() {
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");
        //если сессия зарегестрирована
        if (session("connected") != null) {

            Integer type = Integer.parseInt(DynamicForm.form().bindFromRequest().get("type"));
            Date year = new Date();
            try {
                year = new SimpleDateFormat("yyyy", Locale.ENGLISH).parse(DynamicForm.form().bindFromRequest().get("date"));
            } catch (ParseException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }


            docs docs = new docs();


            if (callbackName != null) {
                JsonContext jsonContext = Ebean.createJsonContext();
                JsonWriteOptions options = new JsonWriteOptions();
                options.setPathProperties("users_d", "users_id, users_fio");
                options.setPathProperties("resolutions_d", "id, pid, docs_r, user_from_r, user_to_r, control_date, text");
                options.setPathProperties("resolutions_d.user_from_r", "users_id, users_fio");
                options.setPathProperties("resolutions_d.user_to_r", "users_id, users_fio");


                return ok(jsonContext.toJsonString(docs.getQuery(type, year, Integer.parseInt(session("id"))), false, options, callbackName));

            } else {
                return ok(callbackName);
            }
        }
        return ok(callbackName);
    }

    public static Result AddLetter() {
        Integer id = Integer.parseInt(ctx().session().get("id"));
        users user = Ebean.find(users.class).setId(id).findUnique();
        // переменная хранит номер входящего, для поиска дубликатов
        String number;
        Integer resol_size;
        List<users> usersList = new ArrayList();

       //String test = DynamicForm.form().bindFromRequest().get("user_to_r[]");
        Set<String> keys = request().body().asMultipartFormData().asFormUrlEncoded().keySet();
        List <String> usersToId = new ArrayList();
        for (String key : keys) {
            if (key.contains("user_to[")){
                int startIndex=key.indexOf('[');
                int endIndex=key.indexOf(']');
                usersToId.add(key.substring(startIndex + 1, endIndex));

            }
        }

        for (int i = 0; i < usersToId.size(); i++) {
            usersList.add(i, Ebean.find(users.class).setId(Integer.parseInt(usersToId.get(i))).findUnique());
        }
        if (usersList.size()>0){
            resol_size=usersList.size();
        }
        else{
            resol_size=1;
        }
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        if (userstatus == controllers.Status.MANAGER) {
            Boolean hasBeenRegAlready = false;
            String contragent = DynamicForm.form().bindFromRequest().get("docs_contr");
            if (DynamicForm.form().bindFromRequest().get("docs_number_in") != null) {
                number = DynamicForm.form().bindFromRequest().get("docs_number_in");
                hasBeenRegAlready = docs.findPrevReg(number, contragent);


            }
            //Проверка на наличиие уже зарегестрированного Вх за данным номеров и контрагентом

            if (hasBeenRegAlready) {
                return ok("AlreadyReg#true");
            } else {


                Http.MultipartFormData body = request().body().asMultipartFormData();
                Integer type = Integer.parseInt(DynamicForm.form().bindFromRequest().get("docs_type"));
                Http.MultipartFormData.FilePart letter = body.getFile("file");


                if (letter != null) {
                    String fileName = letter.getFilename();
                    int dotPos = fileName.lastIndexOf(".") + 1;

                    String ext = fileName.substring(dotPos);
                    if (ext.equals("pdf")) {


                        File file = letter.getFile();
                        String hashfilename = UUID.randomUUID().toString();
                        file.renameTo(new File("../DokKy/public/uploads/" + hashfilename + "." + ext));
                        Form<docs> addOutLetterForm = form(docs.class);
                        Form<resolutions> addOutLetterRes = form(resolutions.class);

                        docs newletter = addOutLetterForm.bindFromRequest().get();
                        for (int i=0 ; i<resol_size; i++){

                         resolutions res = addOutLetterRes.bindFromRequest().get();
                            if (!usersList.isEmpty()){
                                res.user_to_r=usersList.get(i);
                                newletter.resolutions_d.add(res);
                            } else{
                                newletter.resolutions_d.add(res);
                            }

                        if (type == 1) {                     //Исходящее
                            users userCode = new users();
                            //Получаем код подразделения
                            String code = userCode.getCode(newletter.users_d.users_id);
                            //1 в параметре это тип документа, то есть исходящее письмо
                            newletter.docs_number = code + "/" + String.valueOf(newletter.getOutNumber(newletter.docs_date, type) + 1);
                        } else if (type == 0) {               //Входящее
                            newletter.docs_number = String.valueOf(newletter.getOutNumber(newletter.docs_date, type) + 1);

                        }
                        newletter.docs_scan_id = hashfilename + ".pdf";
                        if (!usersList.isEmpty()) {
                            newletter.docs_done = false;
                        } else {
                            newletter.docs_done = true;
                        }


                        newletter.save();
                            if (!usersToId.isEmpty()) {
                            sendEmailNotif email = new sendEmailNotif();
                            users userTo = Ebean.find(users.class).setId(res.user_to_r.users_id).findUnique();
                            email.send(userTo.users_email, newletter.docs_number, newletter.docs_subject, userTo.users_fio, res.text, newletter.docs_contr, newletter.docs_id, res.control_date, 0);
                        }
                        if (newletter.docs_status==true&&type==1) {
                                sendEmailNotif email = new sendEmailNotif();
                                users userTo = Ebean.find(users.class).setId(newletter.users_d.users_id).findUnique();

                                email.send(userTo.users_email, newletter.docs_number, newletter.docs_subject, userTo.users_fio, res.text, newletter.docs_contr, newletter.docs_id, res.control_date, 0);
                        }
                        logger.info(user.users_login + " has added document number" + newletter.docs_number + "doc_id: " + newletter.docs_id);
                        }

                        return ok(newletter.docs_number);
                    } else {
                        return ok("falseFormat");
                    }


                } else {


                    Form<docs> addOutLetterForm = form(docs.class);
                    Form<resolutions> addOutLetterRes = form(resolutions.class);
                    docs newletter = addOutLetterForm.bindFromRequest().get();

                    if (type == 1) {                     //Исходящее
                        users userCode = new users();
                        //Получаем код подразделения
                        String code = userCode.getCode(newletter.users_d.users_id);
                        //1 в параметре это тип документа, то есть исходящее письмо
                        newletter.docs_number = code + "/" + String.valueOf(newletter.getOutNumber(newletter.docs_date, type) + 1);
                    } else if (type == 0) {               //Входящее
                        newletter.docs_number = String.valueOf(newletter.getOutNumber(newletter.docs_date, type) + 1);

                    }
                    for (int i=0 ; i<resol_size; i++){
                        resolutions res = addOutLetterRes.bindFromRequest().get();

                    if (!usersList.isEmpty()) {
                        newletter.docs_done = false;
                    } else {
                        newletter.docs_done = true;
                    }

                        if (!usersList.isEmpty()){
                        res.user_to_r=usersList.get(i);
                        newletter.resolutions_d.add(res);
                        } else{
                            newletter.resolutions_d.add(res);
                        }



                    newletter.save();
                    if (!usersToId.isEmpty()) {
                        sendEmailNotif email = new sendEmailNotif();
                        users userTo = Ebean.find(users.class).setId(usersToId.get(0)).findUnique();
                        email.send(userTo.users_email, newletter.docs_number, newletter.docs_subject, userTo.users_fio, res.text, newletter.docs_contr, newletter.docs_id, res.control_date, 0);
                    }
                        if (newletter.docs_status==true&&type==1) {
                            sendEmailNotif email = new sendEmailNotif();
                            users userTo = Ebean.find(users.class).setId(newletter.users_d.users_id).findUnique();
                            email.send(userTo.users_email, newletter.docs_number, newletter.docs_subject, userTo.users_fio, res.text, newletter.docs_contr, newletter.docs_id, res.control_date, 0);
                        }

                    }
                    logger.info(user.users_login + " has added document number" + newletter.docs_number + "doc_id: " + newletter.docs_id);
                    //.save();
                    return ok(newletter.docs_number);
                }
            }
        } else {
            return ok("Error");
        }

    }

    public static Result EditInLetter() {
        Integer id = Integer.parseInt(ctx().session().get("id"));
        users user = Ebean.find(users.class).setId(id).findUnique();
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        if (userstatus == controllers.Status.MANAGER) {
            String subject = DynamicForm.form().bindFromRequest().get("docs_subject");
            String number = DynamicForm.form().bindFromRequest().get("docs_number_in");
            String contragent = DynamicForm.form().bindFromRequest().get("docs_contr");
            String resolution = DynamicForm.form().bindFromRequest().get("text");
            String answer = DynamicForm.form().bindFromRequest().get("docs_answer");
            Date date = null;
            try {
                date = new SimpleDateFormat("dd/MM/yyyy").parse(DynamicForm.form().bindFromRequest().get("docs_date"));
            } catch (ParseException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            Integer docs_id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("docs_id"));

            Http.MultipartFormData body = request().body().asMultipartFormData();
            Http.MultipartFormData.FilePart letter = body.getFile("file");

            docs newletter = Ebean.find(docs.class).setId(docs_id).findUnique();


            if (letter != null) {
                if (newletter.docs_scan_id == null) {
                    String fileName = letter.getFilename();
                    int dotPos = fileName.lastIndexOf(".") + 1;

                    String ext = fileName.substring(dotPos);
                    if (ext.equals("pdf")) {


                        File file = letter.getFile();
                        String hashfilename = UUID.randomUUID().toString();
                        file.renameTo(new File("../DokKy/public/uploads/" + hashfilename + "." + ext));
                        newletter.docs_scan_id = hashfilename + ".pdf";
                    } else {
                        return ok("falseFormat");
                    }
                } else {
                    String fileName = letter.getFilename();
                    int dotPos = fileName.lastIndexOf(".") + 1;

                    String ext = fileName.substring(dotPos);
                    if (ext.equals("pdf")) {
                        File file = letter.getFile();
                        file.renameTo(new File("../DokKy/public/uploads/" + newletter.docs_scan_id));
                    } else {
                        return ok("falseFormat");
                    }
                }
            }
            newletter.docs_number_in = number;
            newletter.docs_subject = subject;
            newletter.docs_contr = contragent;
            newletter.resolutions_d.get(0).text = resolution;
            newletter.docs_answer = answer;
            newletter.docs_date = date;
            newletter.update();
            logger.info(user.users_login + " has edited document number" + newletter.docs_number);
            return ok(newletter.docs_number);
        } else {
            return ok("Error");
        }


    }

    public static Result EditOutLetter() {
        Integer id = Integer.parseInt(ctx().session().get("id"));
        users user = Ebean.find(users.class).setId(id).findUnique();
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        if (userstatus == controllers.Status.MANAGER) {
            String subject = DynamicForm.form().bindFromRequest().get("docs_subject");
            String contragent = DynamicForm.form().bindFromRequest().get("docs_contr");
            //String resolution=DynamicForm.form().bindFromRequest().get("text").toString();
            String answer = DynamicForm.form().bindFromRequest().get("docs_answer");

            Date date = null;
            try {
                date = new SimpleDateFormat("dd/MM/yyyy").parse(DynamicForm.form().bindFromRequest().get("docs_date"));
            } catch (ParseException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            Integer docs_id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("docs_id"));

            Http.MultipartFormData body = request().body().asMultipartFormData();
            Http.MultipartFormData.FilePart letter = body.getFile("file");
            users ispolnit=null;

            docs newletter = Ebean.find(docs.class).setId(docs_id).findUnique();
            if (!DynamicForm.form().bindFromRequest().get("users_d.users_id").isEmpty()){
                ispolnit=Ebean.find(users.class).setId(Integer.parseInt(DynamicForm.form().bindFromRequest().get("users_d.users_id"))).findUnique();
            }

            if (letter != null) {
                if (newletter.docs_scan_id == null) {
                    String fileName = letter.getFilename();
                    int dotPos = fileName.lastIndexOf(".") + 1;

                    String ext = fileName.substring(dotPos);
                    if (ext.equals("pdf")) {


                        File file = letter.getFile();
                        String hashfilename = UUID.randomUUID().toString();
                        file.renameTo(new File("../DokKy/public/uploads/" + hashfilename + "." + ext));
                        newletter.docs_scan_id = hashfilename + ".pdf";
                    } else {
                        return ok("falseFormat");
                    }
                } else {
                    String fileName = letter.getFilename();
                    int dotPos = fileName.lastIndexOf(".") + 1;

                    String ext = fileName.substring(dotPos);
                    if (ext.equals("pdf")) {
                        File file = letter.getFile();
                        file.renameTo(new File("../DokKy/public/uploads/" + newletter.docs_scan_id));
                    } else {
                        return ok("falseFormat");
                    }
                }
            }
            if (ispolnit!=null){
            newletter.users_d=ispolnit;
            }
            newletter.docs_subject = subject;
            newletter.docs_contr = contragent;
            //newletter.resolutions_d.get(0).text=resolution;
            newletter.docs_answer = answer;
            newletter.docs_date = date;
            newletter.update();
            logger.info(user.users_login + " has edited document number" + newletter.docs_number);
            return ok(newletter.docs_number);
        } else {
            return ok("Error");
        }


    }

    public static Result GetScan() {
        Integer docs_id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("docs_id"));
        docs doc = Ebean.find(docs.class).setId(docs_id).findUnique();
        GetRule getRule = new GetRule(docs_id, Integer.parseInt(session("id")));
        userstatus = new UserStatus(Integer.parseInt(session("id"))).get();
        boolean hasRule=getRule.get();

        if ((doc.docs_scan_id != null) && (userstatus==controllers.Status.BOSS || userstatus== controllers.Status.MANAGER || hasRule==true)) {
            File file = new File("../DokKy/public/uploads/" + doc.docs_scan_id);
            response().setContentType("application/pdf");
            response().setHeader("Content-Disposition", "attachment; filename*=\"utf-8'" + doc.docs_scan_id + "\"");

            response().setHeader("Content-Length", String.valueOf(file.length()));
            return ok(file);
        } else {
        return ok("Error");
        }
    }

    public static Result DelScan() {
        Integer docs_id = Integer.parseInt(DynamicForm.form().bindFromRequest().get("docs_id"));
        docs doc = Ebean.find(docs.class).setId(docs_id).findUnique();
        String callbackName = DynamicForm.form().bindFromRequest().get("callback");

        if (doc.docs_scan_id != null) {
            File file = new File("../DokKy/public/uploads/" + doc.docs_scan_id);
            file.delete();
            doc.docs_scan_id = null;
            doc.update();
        }
        if (callbackName != null) {
            // JSONP wrapping:
            callbackName = callbackName +
                    "(" + Json.toJson(doc) + ")";
        }
        return ok(callbackName);
    }
}
