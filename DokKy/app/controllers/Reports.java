package controllers;

import models.docs;
import org.apache.poi.hssf.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 01.08.13
 * Time: 15:13
 * Класс отвечает за выгрузку различных отчетов в Excel при помощи Apache Poi
 */
public class Reports {
    private Workbook wb;
    private  FileOutputStream fileOut;
    private Sheet sheet;
    private CellStyle headerStyle;
    private CellStyle usualStyle;
    private CellStyle dateStyle;



    public Reports(){
        wb = new HSSFWorkbook();
        sheet = wb.createSheet("new sheet");

        setStyles();
    }
    //Отчет журнал корреспонденции
    public void getJournal(List<docs> docs_for_report){

        Row row = sheet.createRow((short)0);
        row.setHeightInPoints(30);
        row.createCell(0).setCellStyle(headerStyle);
        row.getCell(0).setCellValue("№ Вх.");

        row.createCell(1).setCellStyle(headerStyle);
        row.getCell(1).setCellValue("Дата");
        row.createCell(2).setCellStyle(headerStyle);
        row.getCell(2).setCellValue("Автор документа");
        row.createCell(3).setCellStyle(headerStyle);
        row.getCell(3).setCellValue("Ссылка на № и дату");
        row.createCell(4).setCellStyle(headerStyle);
        row.getCell(4).setCellValue("Краткое содержание");
        row.createCell(5).setCellStyle(headerStyle);
        row.getCell(5).setCellValue("Резолюция");
        row.createCell(6).setCellStyle(headerStyle);
        row.getCell(6).setCellValue("Исполнитель");
        row.createCell(7).setCellStyle(headerStyle);
        row.getCell(7).setCellValue("Подпись");

        for (int i=0; i< docs_for_report.size(); i++){

            Row rowContent = sheet.createRow((short)i+1);
            try {
                rowContent.createCell(7).setCellStyle(usualStyle);
            rowContent.createCell(0).setCellStyle(usualStyle);
            if (docs_for_report.get(i).docs_number!=null){
            rowContent.getCell(0).setCellValue(docs_for_report.get(i).docs_number);
            rowContent.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
            }
                rowContent.createCell(1).setCellStyle(dateStyle);
            if (docs_for_report.get(i).docs_date!=null){
                rowContent.getCell(1).setCellValue(docs_for_report.get(i).docs_date);
            }
                rowContent.createCell(2).setCellStyle(usualStyle);
            if (docs_for_report.get(i).docs_contr!=null){
                 rowContent.getCell(2).setCellValue(docs_for_report.get(i).docs_contr);
            }
                rowContent.createCell(3).setCellStyle(usualStyle);
            if (docs_for_report.get(i).docs_number_in!=null){
                rowContent.getCell(3).setCellValue(docs_for_report.get(i).docs_number_in);
            }
                rowContent.createCell(4).setCellStyle(usualStyle);
            if (docs_for_report.get(i).docs_subject!=null){
                rowContent.getCell(4).setCellValue(docs_for_report.get(i).docs_subject);
            }
                rowContent.createCell(5).setCellStyle(usualStyle);
            if (docs_for_report.get(i).resolutions_d.get(0).text!=null){
                rowContent.getCell(5).setCellValue(docs_for_report.get(i).resolutions_d.get(0).text);
            }
                rowContent.createCell(6).setCellStyle(usualStyle);
            if (docs_for_report.get(i).resolutions_d.get(0).user_to_r.users_fio!=null){
                rowContent.getCell(6).setCellValue(docs_for_report.get(i).resolutions_d.get(0).user_to_r.users_fio);
            }

            } catch (NullPointerException ex){

            }




        }

        try {
            fileOut = new FileOutputStream("../DokKy/public/reports/report.xls");
            try {
                wb.write(fileOut);
                fileOut.close();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    private void setStyles(){
        CreationHelper createHelper = wb.getCreationHelper();
        sheet.setColumnWidth(2,6000);
        sheet.setColumnWidth(3,4000);
        sheet.setColumnWidth(4,6500);
        sheet.setColumnWidth(5,4500);
        sheet.setColumnWidth(6,4500);
        sheet.setColumnWidth(7,3000);

        sheet.setRepeatingRows(CellRangeAddress.valueOf("1:1"));

        PrintSetup ps = sheet.getPrintSetup();
        ps.setFitWidth((short) 1);
        ps.setLandscape(true);
        sheet.setMargin(Sheet.LeftMargin, 0.5);
        sheet.setMargin(Sheet.RightMargin, 0.5);
        sheet.setMargin(Sheet.TopMargin, 0.8);
        sheet.setMargin(Sheet.BottomMargin, 0.5);

        Font fontBold = wb.createFont();
        fontBold.setBoldweight(Font.BOLDWEIGHT_BOLD);
        headerStyle = wb.createCellStyle();
        headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
        headerStyle.setBorderBottom(CellStyle.BORDER_THIN);
        headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
        headerStyle.setBorderRight(CellStyle.BORDER_THIN);
        headerStyle.setBorderTop(CellStyle.BORDER_THIN);
        headerStyle.setFont(fontBold);
        headerStyle.setWrapText(true);


        usualStyle = wb.createCellStyle();
        usualStyle.setAlignment(CellStyle.ALIGN_CENTER);
        usualStyle.setBorderBottom(CellStyle.BORDER_THIN);
        usualStyle.setBorderLeft(CellStyle.BORDER_THIN);
        usualStyle.setBorderRight(CellStyle.BORDER_THIN);
        usualStyle.setBorderTop(CellStyle.BORDER_THIN);
        usualStyle.setWrapText(true);

        dateStyle = wb.createCellStyle();
        dateStyle.setAlignment(CellStyle.ALIGN_CENTER);
        dateStyle.setBorderBottom(CellStyle.BORDER_THIN);
        dateStyle.setBorderLeft(CellStyle.BORDER_THIN);
        dateStyle.setBorderRight(CellStyle.BORDER_THIN);
        dateStyle.setBorderTop(CellStyle.BORDER_THIN);
        dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yy"));


    }
}


