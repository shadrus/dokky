package controllers;

import com.avaje.ebean.Ebean;
import models.system_email;
import org.apache.commons.mail.*;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import views.html.mainemail;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 22.05.13
 * Time: 15:47
 * To change this template use File | Settings | File Templates.
 */
public class sendEmailNotif  {
    final static private play.Logger.ALogger logger = play.Logger.of("application");

    // MultiPartEmail email = new MultiPartEmail();
    HtmlEmail email = new HtmlEmail();

    //конфигурация соединения
    public sendEmailNotif(){
        system_email bd_email = Ebean.find(system_email.class).setId(1).findUnique();
        if (bd_email!=null){
        email.setHostName(bd_email.system_smtp);
        email.setAuthenticator(new DefaultAuthenticator(bd_email.system_login, bd_email.system_password));
        try {
            email.setFrom(bd_email.system_address);
        } catch (EmailException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        email.setCharset("utf-8");
        }

    }

    public void send (String address, String number, String subject, String user, String resolution, String contragent, Integer docs_id, Date date, int type){
        String doc_link="http://docs.umek-sk.ru/assets/"+Application.curIndex+"#/doccard/"+docs_id;
        String html=null;

        // Create an instance of SimpleDateFormat used for formatting
        // the string representation of date (month/day/year)
        String strDate;
        if (date!=null){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        // Using DateFormat format method we can create a string
        // representation of a date with the defined format.
            strDate = df.format(date);
        } else{
            strDate = "без контрольного срока";
        }

        try {
            /*
            type:
            0-письмо при регистрации
            1-напоминание об истечении срока
            */
            String body=null;
            if (type==0){
            body = views.html.mainemail.render(subject, number, strDate, doc_link, contragent, resolution).body();
            } else if (type==1){
            body = views.html.controlmail.render(subject, number, strDate, doc_link, contragent, resolution).body();
            }
            // Create the attachment
            email.setSubject("Вам отписано письмо "+subject);
            email.setHtmlMsg(body);
            email.addTo(address.toString());
            email.send();
            logger.info("Send email to "+address.toString());
        } catch (EmailException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
