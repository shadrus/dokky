package controllers;

import com.avaje.ebean.Ebean;
import models.users;

import static play.mvc.Controller.ctx;
import static play.mvc.Controller.session;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 30.07.13
 * Time: 10:12
 * To change this template use File | Settings | File Templates.
 */
public class UserStatus {
    private Status status;
    public UserStatus(Integer id){
        users user = Ebean.find(users.class).setId(id).findUnique();
        switch (user.status_u.statuses_id){
            case 1: status=Status.ADMIN;
                break;
            case 2: status=Status.MANAGER;
                break;
            case 3: status=Status.USER;
                break;
            case 4: status=Status.BOSS;
                break;
            default: status = Status.INVALID;
                break;
        }

    }
    public Status get(){
        return status;
    }

}
