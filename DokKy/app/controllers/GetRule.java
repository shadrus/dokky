package controllers;

import com.avaje.ebean.Ebean;
import models.docs;
import models.users;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 14.08.13
 * Time: 8:08
 * To change this template use File | Settings | File Templates.
 * Имеет ли пользователь право работать с документом*/
public class GetRule {
    private  Integer doc_id;
    private users user;
    public GetRule (Integer doc_id, Integer user_id){
        this.doc_id=doc_id;
        user = Ebean.find(users.class).setId(user_id).findUnique();

    }
    public boolean get (){
        GetResolutionsByDocID res = new GetResolutionsByDocID(doc_id);
        List<Integer> usersId = res.get();
        boolean hasRule=false;
        if (usersId!=null){
            for (int i = 0; i< usersId.size(); i++){
                if (user.users_id==usersId.get(i)){
                    hasRule=true;
                }
            }
        }
        try{
             if (Ebean.find(docs.class).setId(doc_id).findUnique().users_d.users_id==user.users_id){
                  hasRule=true;
             }
        } catch (NullPointerException ex){

        }
        return  hasRule;
    }
}
