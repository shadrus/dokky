import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.avaje.ebean.Ebean;
import controllers.sendEmailNotif;
import models.docs;
import models.system_notifdays;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.libs.Akka;
import scala.concurrent.ExecutionContext;
import scala.concurrent.duration.*;


/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 10.06.13
 * Time: 10:41
 * To change this template use File | Settings | File Templates.
 */
public class Global extends GlobalSettings {

    @Override
    public void onStart(Application app) {
        Akka.system().scheduler().schedule(
                Duration.create(0, TimeUnit.DAYS),
                Duration.create(1, TimeUnit.DAYS),
                new Runnable() {
                    public void run() {
                        system_notifdays days = new system_notifdays();
                        List<docs> doc = docs.findCtrlDay(Ebean.find(system_notifdays.class).setId(1).findUnique().system_days);
                           for (int i=0; i<doc.size(); i++){
                               sendEmailNotif email = new sendEmailNotif();
                               email.send(doc.get(i).resolutions_d.get(0).user_to_r.users_email, doc.get(i).docs_number, doc.get(i).docs_subject, doc.get(i).resolutions_d.get(0).user_to_r.users_fio, doc.get(i).resolutions_d.get(0).text, doc.get(i).docs_contr, doc.get(i).docs_id, doc.get(i).resolutions_d.get(0).control_date, 1);
                           }

                    }
                },
                Akka.system().dispatcher()
        );

    }
}
