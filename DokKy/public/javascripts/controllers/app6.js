/**
 * Created with IntelliJ IDEA.
 * User: krylov
 * Date: 22.04.13
 * Time: 14:06
 * To change this template use File | Settings | File Templates.
 */
var myModule = angular.module('DoKky', ['ui', 'ngResource','ngCookies', 'ngUpload', 'ng']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/inbox', {templateUrl: 'partials/in.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();
                $scope.docType = "Входящая корреспонденция";
                $scope.cat_image = "./images/folder_inbox.png"
            }
            }).
            when('/outbox', {templateUrl: 'partials/out.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();

                $scope.docType = "Исходящая корреспонденция";
                $scope.cat_image = "./images/mail-outbox.png"
            }}).
            when('/settings', {templateUrl: 'partials/settings.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();

                $scope.docType = "Настройка системы";
                $scope.cat_image = "./images/settings.png"
            }}).
            when('/logs', {templateUrl: 'partials/logs.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();

                $scope.docType = "Панель логов";
                $scope.cat_image = "./images/settings.png"
            }}).
            when('/user/:id', {templateUrl: 'partials/usercard.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();

                $scope.docType = "Карточка сотрудника";
                $scope.cat_image = "./images/users.png"
            }}).
            when('/users', {templateUrl: 'partials/users.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();

                $scope.docType = "Настройка пользователей";
                $scope.cat_image = "./images/settings.png"
            }}).
            when('/otdels', {templateUrl: 'partials/otdels.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();

                $scope.add = false;
                $scope.docType = "Настройка подразделений";
                $scope.cat_image = "./images/settings.png"
            }}).
            when('/dolg', {templateUrl: 'partials/dolg.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();

                $scope.docType = "Настройка должностей";
                $scope.cat_image = "./images/settings.png"
            }}).
            when('/doccard/:id', {templateUrl: 'partials/doccard.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();

                $scope.docType = "Карточка документа";
                $scope.cat_image = "./images/edit-notes.png"
            }}).
            when('/newin', {templateUrl: 'partials/newin.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();

            }}).
            when('/editin/:id', {templateUrl: 'partials/editin.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();
                $scope.cat_image = "./images/edit-notes.png"

            }}).
            when('/resolution/:id', {templateUrl: 'partials/resolution.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();
                $scope.cat_image = "./images/edit-notes.png"
            }}).
            when('/editout/:id', {templateUrl: 'partials/editout.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();
                $scope.cat_image = "./images/edit-notes.png"

            }}).
            when('/newout', {templateUrl: 'partials/newout.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();
            }}).
            when('/edituser/:id', {templateUrl: 'partials/edituser.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();
            }}).
            when('/adduser', {templateUrl: 'partials/adduser.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();
            }}).
            when('/reports', {templateUrl: 'partials/reports.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.getSession();
                $scope.showMenU();
            }}).
            when('/login', {templateUrl: 'partials/login.html?' + (Math.random() * (1 - 0) + 0), controller: function ($scope) {
                $scope.hideMenU();
            }}).
            otherwise({redirectTo: '/login?'+ (Math.random() * (1 - 0) + 0)});
    }]);

//Подключаю календарь
/*myModule.value('ui.config', {
    date: {
        allowClear: true,
        dateFormat: "dd/mm/yy"

    }

});*/


myModule.directive('customDate', ['$timeout', function (timer){
    return{
    link: function (scope, element, attrs) {
            var hello = function () {
            element.datepicker({
            dateFormat: 'dd/mm/yy', allowClear: true,
            beforeShowDay: function (date) {
                var day = moment(date.getTime()).format("DD/MM/YYYY");
                var c_date = scope.docsControlled(day);
                if (c_date.length>0){
                           return [true, 'Highlighted', c_date.length+' на контроле'];
                   console.log(c_date);
                }
                else{
                    return [true, '', ''];
                }

            },
                onSelect:function (dateText) {
                   // var day = moment(dateText.getTime()).format("DD/MM/YYYY");
                   // scope.$apply(function(scope){

                        // Change binded variable
                   //     ngModel.assign(scope, dateText);
                   // });
                    var modelPath = $(this).attr('ng-model');
                    putObject(modelPath, scope, dateText);
                    scope.$apply();
                }
        });
        }

                timer(hello, 1000);

    }
    }
}])
function putObject(path, object, value) {
    var modelPath = path.split(".");

    function fill(object, elements, depth, value) {
        var hasNext = ((depth + 1) < elements.length);
        if(depth < elements.length && hasNext) {
            if(!object.hasOwnProperty(modelPath[depth])) {
                object[modelPath[depth]] = {};
            }
            fill(object[modelPath[depth]], elements, ++depth, value);
        } else {
            object[modelPath[depth]] = value;
        }
    }
    fill(object, modelPath, 0, value);
}

function Inbox($scope, $resource, $templateCache, $routeParams, $location, $route, $parse, $cookieStore) {
    //массив хранения ответсвенных по резолюции
    $scope.otvets = [];
    //Авторизация
    var myDate = ['23/06/13'];
    $scope.login_query = $resource('/login',
        {callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: false}});
    //Наличие сессии
    $scope.session_query = $resource('/getsession',
        {callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //Выход
    $scope.logout_query = $resource('/logout',
        {callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: false}});
    //Отделы
    $scope.otdels_query = $resource('/getOtdels',
        {callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //Должности
    $scope.dolgnes_query = $resource('/getDolgn',
        {callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    $scope.dolgnes_id_query = $resource('/:action',
        {action: "getDolgnId", otdel_id: "", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //Пользователи
    $scope.users_query = $resource('/getUsers',
        {callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //Карточка сотрудника
    $scope.usercard_query = $resource('/getUserCard',
        {callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //Статусы пользователей
    $scope.statuses_query = $resource('/getStatuses',
        {callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //Добавляем добавляем пользователя
    $scope.users_query_add = $resource('/:action',
        {action: "addUser"},
        {save: {method: 'POST', isArray: true}});
    //удаляем пользователя
    $scope.users_query_del = $resource('/:action',
        {action: "delUser", users_id: "", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //Добавляем отдел
    $scope.otdels_query_add = $resource('/:action',
        {action: "addOtdel", otdel_name: "", otdel_code: "", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: false}});
    //Добавляем должность
    $scope.dolgn_query_add = $resource('/:action',
        {action: "addDolgn", dolgn_name: "", otdel_id: "", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: false}});
    //удаляем должность
    $scope.dolgns_query_del = $resource('/:action',
        {action: "delDolgn", dolgn_id: "", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //добавляем исходящее письмо
    $scope.outletter_query_add = $resource('/:action',
        {action: "addoutletter", callback: "JSON_CALLBACK"},
        {post: {method: 'JSONP', isArray: true}});
    //Получаем список документов
    //type - тип документа
    $scope.docs_query_get = $resource('/getDocs',
        {type: "", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //Карточка документа

    $scope.doccard_query = $resource('/getDocCard',
        {callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //удаляем документ
    $scope.docs_query_del = $resource('/:action',
        {action: "delDoc", callback: "JSON_CALLBACK"},
        {query: {method: 'JSONP', isArray: false}});
    //Дата самого первого документа для архива
    $scope.docs_firstdate_query = $resource('/getFirstDate',
        {callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: false}});
    //добавляем системный email
    $scope.systemmail_query_add = $resource('/:action',
        {action: "addsystememail", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: false}});
    //Получаем настройки ситемного email
    $scope.systemmail_query_get = $resource('/getsystememail',
        {callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //устанавливаем контроль
    $scope.setctrldate_query_add = $resource('/:action',
        {action: "setcontroldate", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //удаляем контроль
    $scope.delctrldate_query_add = $resource('/:action',
        {action: "delcontroldate", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: false}});
    //меняем статус на исполнено
    $scope.setisp_query = $resource('/:action',
        {action: "setisp", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //редактируем должность
    $scope.dolgn_query_edit = $resource('/:action',
        {action: "editDolgn", callback: "JSON_CALLBACK"},
        {query: {method: 'JSONP', isArray: true}});
    //редактируем отдел
    $scope.otdel_query_edit = $resource('/:action',
        {action: "editOtdel", callback: "JSON_CALLBACK"},
        {query: {method: 'JSONP', isArray: true}});
    //редактируем количество дней, за которые необходимо начинать напоминать о контроле
    $scope.notif_query_set = $resource('/:action',
        {action: "editnotifdays", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //редактируем количество дней, за которые необходимо начинать напоминать о контроле
    $scope.notif_query_get = $resource('/:action',
        {action: "getnotifdays", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: false}});
    //редактируем количество дней, за которые необходимо начинать напоминать о контроле
    $scope.getlog_get = $resource('/:action',
        {action: "getlog", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: true}});
    //редактируем количество дней, за которые необходимо начинать напоминать о контроле
    $scope.delscan_get = $resource('/:action',
        {action: "delscan", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: false}});
    //Добавляем резолюцию
    $scope.resol_query_add = $resource('/:action',
        {action: "addResol", callback: "JSON_CALLBACK"},
        {get: {method: 'JSONP', isArray: false}});




    $scope.hideMenU = function () {
        $templateCache.removeAll();
        $scope.menu = false;
    }
    $scope.showMenU = function () {
        $templateCache.removeAll();
        $scope.menu = true;
        $scope.cat_image = "./images/document_add.png"
    }

    $scope.getlog = function () {
        $scope.log = $scope.getlog_get.get();
    }



    //Функция определения начала данных на странице
    $scope.start = function (data) {
        $scope.startItem = data;
        //очищаем фильтр, если он был
        $scope.filter = "";
        $scope.currentPage = data / $scope.limit;


    }

    //скрываем лишнее
    $scope.showAdmin = function () {
        $scope.show_admin = $scope.admin;
    }


    //Авторизация
    $scope.getAuth = function () {
        $scope.loginmes = false;
        if (typeof $scope.remember_me == 'undefined'){
            $scope.remember_me=false;
        }
        $scope.isAuth = $scope.login_query.get({login: $scope.login, password: $scope.password}, function () {
            if ($scope.isAuth.result == "0") {
                $scope.loginmes = true;
                $scope.loginMes = "Неверно имя пользователя или пароль";
            } else {

                $scope.user_status = $scope.isAuth.result;
                $scope.user_id=$scope.isAuth.id;
                //Администратор
                if ($scope.user_status == 1) {
                    $scope.admin = true;
                    $scope.show_manager = false;
                    $scope.show_boss = false;
                    //делопроизводитель
                } else  if ($scope.user_status == 2) {
                    $scope.admin = false;
                    $scope.show_manager = true;
                    $scope.show_boss = false;
                    //пользователь
                } else  if ($scope.user_status == 3) {
                    $scope.admin = false;
                    $scope.show_manager = false;
                    $scope.show_boss = false;

                    //руководство
                } else  if ($scope.user_status == 4) {
                    $scope.admin = false;
                    $scope.show_manager = false;
                    $scope.show_boss = true;

                }
                // если в кукисах сохранен докайди, то человек пытался войти неавторизованным на карточку и его перекинуло на авторизацию
                var value = $cookieStore.get('docId');
                if (value!=null){

                     $cookieStore.remove('docId');
                     $location.path('/doccard/'+value);
                } else{
                $location.path('/inbox');
                }
            }
        });

        $scope.cat1 = false;
        $templateCache.removeAll();

    }

    // боремся с кэшированием
    $scope.removeCache = function () {
        $templateCache.removeAll();
    }

    //Если резолюцию зхотел ставить руководитель
    $scope.addResol = function (otvets, resoltext, control_date, resol_from_id) {
        var currentArr = [];
        for (var i=0; i<otvets.length; i++){
             currentArr.push(otvets[i].users_id);
        }

        if (typeof control_date != 'undefined'){
        var date_formated=moment(control_date).format("DD/MM/YYYY");
        } else {
            var date_formated=null;
        }

        if (typeof resol_from_id == 'undefined'){
            resol_from_id=null;
        }
        $scope.isResolSuc = $scope.resol_query_add.get({resol_from: resol_from_id, docs_id: $routeParams.id, control_date:  date_formated, otvet: currentArr, text: resoltext}, function(){
            $location.path('/doccard/'+$routeParams.id);
        });
        $scope.cat1 = false;
        $templateCache.removeAll();

    }


    //Авторизация
    $scope.getSession = function () {
        $scope.isSession = $scope.session_query.get(function () {
            if ($scope.isSession[0].isAuth == false) {
                if($routeParams.id!=null){
                    $cookieStore.put('docId', $routeParams.id);
                }
                $location.path('/login');

            }
            //В зависимости от типа пользователя скрываем ненужные ему блоки
            else {

                if ($scope.isSession[0].user_status == 1) {
                    $scope.show_admin = true;
                    $scope.show_manager = false;
                    $scope.show_boss = false;
                } else if ($scope.isSession[0].user_status == 2) {
                    $scope.show_manager = true;
                    $scope.show_admin = false;
                    $scope.show_boss = false;
                }  else  if ($scope.isSession[0].user_status == 3) {
                    $scope.show_manager = false;
                    $scope.show_admin = false;
                    $scope.show_boss = false;
                }
                else  if ($scope.isSession[0].user_status == 4) {
                    $scope.show_manager = false;
                    $scope.show_admin = false;
                    $scope.show_boss = true;
                }
            }
        });
    }

    //Выход
    $scope.logout = function () {
        $scope.loginmes = false;
        $scope.isAuthOut = $scope.logout_query.get(function () {
            if ($scope.isAuthOut.result == 1) {
                $location.path('/login');
            }
        });

        $scope.cat1 = false;
        $templateCache.removeAll();

    }

    //Список пользователей
    $scope.getUsers = function () {
        $scope.currentPage = 0;
        $scope.startItem = 0;
        $scope.limit = 30;
        $scope.numberOfPages = [];
        $scope.loader = true;
        $scope.users = $scope.users_query.get(function () {
            if ($scope.users[0].isAuth == false) {
                $scope.loginmes = true;
                $scope.loginMes = "Вы не имеете доступ к данной странице";
                $location.path('/login')

            } else {
                $scope.count = Math.ceil($scope.users.length / $scope.limit);
                $scope.numberOfPages = [];

                for (var i = 0; i < $scope.count; i++) {
                    $scope.numberOfPages.push(i);
                }
                $scope.loader = false;
            }
        });

        $scope.cat1 = false;
        $templateCache.removeAll();

    }

    //Список пользователей
    $scope.getUserCard = function () {
        $scope.loader = true;
        //получаю карточку пользователя
        $scope.userCard = $scope.usercard_query.get({users_id: $routeParams.id}, function success() {
            $scope.loader = false;
            //получаю списко отделов и определяю к какому относится пользователь, чтобы выбелить в селекте
            $scope.otdels = $scope.otdels_query.get(function () {
                for (var i = 0; i < $scope.otdels.length; i++) {
                    if ($scope.otdels[i].otdel_id == $scope.userCard[0].dolgn_u.otdel_d.otdel_id) {
                        $scope.selectedOtdel = $scope.otdels[i];
                        $scope.dolgns = $scope.dolgnes_id_query.get({otdel_id: $scope.otdels[i].otdel_id});
                        //получаю списко должностей отдела и определяю к какому относится пользователь, чтобы выбелить в селекте
                        $scope.dolgns = $scope.dolgnes_query.get(function () {
                            for (var a = 0; a < $scope.dolgns.length; a++) {
                                if ($scope.dolgns[a].dolgn_id == $scope.userCard[0].dolgn_u.dolgn_id) {
                                    $scope.selectedDolgn = $scope.dolgns[a];
                                    //также получаем статус пользователя
                                    $scope.statuses = $scope.statuses_query.get(function () {
                                        for (var b = 0; b < $scope.statuses.length; b++) {
                                            if ($scope.statuses[b].statuses_id == $scope.userCard[0].status_u.statuses_id) {
                                                $scope.selectedStatus = $scope.statuses[b];
                                            }
                                        }
                                    });

                                }
                            }
                        });


                    }
                }

            });


        });
        $scope.cat1 = false;
        $templateCache.removeAll();

    }

    //Список годов архива
    $scope.getFirstDate = function (type) {

        $scope.Years = [];
        $scope.getYear = $scope.docs_firstdate_query.get({type: type}, function () {
            var currentYear = moment().format("YYYY");
            var firstDate = $scope.getYear.firstdate;
            for (i = parseInt(firstDate); i <= parseInt(currentYear); i++) {
                $scope.Years.push(i)
            }
        });


        $scope.cat1 = false;
        $templateCache.removeAll();

    }

    //Список годов архива
    $scope.delScan = function () {

        $scope.result = $scope.delscan_get.get({docs_id: $routeParams.id}, function () {
            $scope.openfilelink=false;
        });

    }


    //Список документов
    $scope.getDocs = function (type, date) {
        $templateCache.removeAll();
        if (typeof date == 'undefined') {
            $scope.dateC = moment().format("YYYY");
        } else {
            $scope.dateC = date;
        }
        $scope.dateCtrl = moment().format("DD/MM/YYYY");
        $scope.currentPage = 0;
        $scope.startItem = 0;
        $scope.limit = 20;
        $scope.numberOfPages = [];
        $scope.loader = true;
        $scope.docs = $scope.docs_query_get.get({type: type, date: $scope.dateC}, function () {

            $scope.count = Math.ceil($scope.docs.length / $scope.limit);
            $scope.numberOfPages = [];

            for (var i = 0; i < $scope.count; i++) {
                $scope.numberOfPages.push(i);
            }
            for (var i = 0; i < $scope.docs.length; i++) {
                if ($scope.docs[i].docs_status == true) {
                    $scope.docs[i].docs_status = 'K';
                }
                else {
                    $scope.docs[i].docs_status = '';
                }

            }
            $scope.loader = false;


        });
        if (typeof $scope.docs[0] == "undefined") {
            $scope.loader = false;
        }


        $scope.cat1 = false;
        $templateCache.removeAll();

    }
    //Карточка документа
    $scope.getDocById = function () {
        $scope.loader = true;
        $scope.docCard = $scope.doccard_query.get({docs_id: $routeParams.id}, function () {
            $scope.showCtrl = $scope.docCard[0].docs_status;
            if ($scope.docCard[0].resolutions_d[0].user_to_r!=null){
                for(var i=0; i<$scope.docCard[0].resolutions_d.length; i++){
                if (($scope.docCard[0].resolutions_d[i].user_to_r.users_id==$scope.isSession[0].id)&&($scope.docCard[0].docs_done==false)){
                       $scope.showIsp=true;
                     }
                }
            }
            if ($scope.docCard[0].docs_scan_id == null) {
                $scope.openfilelink = false;
            }
            else {
                $scope.openfilelink = true;
            }
            if ($scope.docCard[0].docs_type==0){
                $scope.doc_type="editin"
            } else if ($scope.docCard[0].docs_type==1){
                $scope.doc_type="editout"
            }
            $scope.loader = false;
        });

        $scope.cat1 = false;
        $templateCache.removeAll();

    }


    $scope.docsControlled = function (dateC) {
        var a = moment(dateC, ["DD/MM/YYYY"]).valueOf();
        var datetest;
        var items = [];
        for (var i = 0; i < $scope.docs.length; i++) {

            if (($scope.docs[i].docs_status === "K") && ($scope.docs[i].resolutions_d[0].control_date!=null)) {
                datetest = moment($scope.docs[i].resolutions_d[0].control_date, ["YYYY-MM-DD"]).add("days", 1).valueOf();
                if (datetest == a) {
                    items.push($scope.docs[i]);
                  }

            }
            if (items.length==0){
                $scope.showCtlText=false;
            }  else{
                $scope.showCtlText=true;
            }
        }

        return items;
    }
    //окрашиваем контрольные строки
    $scope.isControlledStyle = function (doc) {
        if (doc.docs_status === "K") {
            return {color: "red"}
        }
    }

    //Открываем окно исполнения
    $scope.ShowIspModal = function () {
                $('#myModalSetIsp').foundation('reveal', 'open');

    }
    //меняем статус документа на исполнено
    $scope.SetIsp = function () {
        var result = $scope.setisp_query.get({docs_id: $routeParams.id, res_report: $scope.res_report}, function () {
            if (result != "Error") {
                $('#myModalSetIsp').foundation('reveal', 'close');
                for (var i=0; i<$scope.docCard[0].resolutions_d.length; i++){
                    if ($scope.docCard[0].resolutions_d[i].user_to_r.users_id==$scope.isSession[0].id){
                     $scope.docCard[0].resolutions_d[i].res_report=$scope.res_report;
                    }
                }


            }
        });
    }

    //получаем системный email
    $scope.getSystemEmail = function () {
        email = $scope.systemmail_query_get.get(function () {
            $scope.system_email = email[0];


        });

    }
    //добавляем системный email
    $scope.SetEmail = function () {
        var result = $scope.systemmail_query_add.get({system_smtp: $scope.system_email.system_smtp, system_password: $scope.system_email.system_password, system_login: $scope.system_email.system_login, system_address: $scope.system_email.system_address }, function () {
            if (result != "Error") {
                $scope.messageSet = "Данные успешно изменены";
                $('#mySettings').foundation('reveal', 'open');
            }
        });
    }
    //получаем системный email
    $scope.getSystemEmail = function () {
        email = $scope.systemmail_query_get.get(function () {
            $scope.system_email = email[0];


        });

    }
    //добавляем контрольную дату
    $scope.setCtrlDate = function (date) {
        date = moment(date).format("DD/MM/YYYY");
        $scope.setctrldate_query_add.get({docs_id: $routeParams.id, ctrl_date: date}, function (data) {
            $('#myModalSetConrol').foundation('reveal', 'close');
            $route.reload();
        });
    }
    //добавляем контрольную дату
    $scope.setCtrlDateModal = function () {
        $('#myModalSetConrol').foundation('reveal', 'open');

    }
    //устанавливаем срок начала рассылки напоминаний о контроле
    $scope.SetNotif = function () {
        var result = $scope.notif_query_set.get({system_days: $scope.system_days}, function () {
            if (result != "Error") {
                $scope.messageSet = "Данные успешно изменены";
                $('#mySettings').foundation('reveal', 'open');
            }
        });

    }
    //закрываем любое модальное окно по имени id
    $scope.closeModal = function (modalname) {
        $(modalname).foundation('reveal', 'close');

    }
    //устанавливаем срок начала рассылки напоминаний о контроле
    $scope.getCurNotif = function () {
        var result = $scope.notif_query_get.get(function () {
            $scope.system_days = result.system_days;

        });
    }
    //удаляем контроль
    $scope.delCtrlDate = function () {
        $scope.delctrldate_query_add.get({docs_id: $routeParams.id}, function (data) {
            $route.reload();
        });
    }
    //добавляем пользователя
    $scope.addUser = function () {
        $scope.users_query_add.save({users_login: $scope.users_login, users_password: $scope.users_password, 'status_u.statuses_id': $scope.status_u.statuses_id, 'dolgn_u.dolgn_id': $scope.dolgn_u.dolgn_id, users_email: $scope.users_email, users_fio: $scope.users_fio, users_phone1: $scope.users_phone1, users_phone2: $scope.users_phone2, users_phone3: $scope.users_phone3, photo: $scope.photo }, function () {
            $route.reload();
        });

    }

    //удаляем пользователя
    $scope.delUser = function () {
        $scope.users_query_del.query({users_id: $routeParams.id}, function () {
            $location.path('/users');
        });

    }

    //запускаем модальное окно редактирования должности
    $scope.showEditModal = function (dolg, index, type) {
        $scope.modal_index = index;
        if (type == 1) {
            $scope.modal_dolgn_id = dolg.dolgn_id;
            $scope.modal_dolgn_name = dolg.dolgn_name;
            $('#modalEditDolgn').foundation('reveal', 'open');
        } else if (type == 0) {
            $scope.modal_otdel_id = dolg.otdel_id;
            $scope.modal_otdel_name = dolg.otdel_name;
            $scope.modal_otdel_code = dolg.otdel_code;
            $('#modalEditOtdel').foundation('reveal', 'open');

        }
    }
    //редактируем должность
    $scope.editDolgn = function () {
        $scope.dolgn_query_edit.query({dolgn_id: $scope.modal_dolgn_id, dolgn_name: $scope.modal_dolgn_name}, function success() {
                $scope.dolgns[$scope.modal_index].dolgn_name = $scope.modal_dolgn_name;
                $('#modalEditDolgn').foundation('reveal', 'close');
            },
            function error() {
                alert("error");
            });

    }
    $scope.getDolgnes = function () {

        $scope.currentPage = 0;
        $scope.startItem = 0;
        $scope.limit = 30;
        $scope.numberOfPages = [];
        $scope.loader = true;
        $scope.dolgns = $scope.dolgnes_query.get(function () {
            $scope.count = Math.ceil($scope.dolgns.length / $scope.limit);
            for (var i = 0; i < $scope.count; i++) {
                $scope.numberOfPages.push(i);
            }
            $scope.loader = false;
        });
        $scope.cat = false;
        $templateCache.removeAll();

    }


    $scope.addOutLetter = function () {
        $scope.outletter_query_add.query({docs_subject: $scope.docs_subject, file: $scope.file, docs_contr: $scope.contr_d, docs_date: $scope.docs_date, docs_status: "0", docs_type: "1", docs_answer: $scope.docs_answer,
                text: $scope.resolution_text, control_date: $scope.resolution_control_date, 'user_from_r.users_id': $scope.resol.users_id, 'user_to_r.users_id': $scope.otvet.users_id}

        );

    }

    //выбираем исполнителя по письму
    $scope.setIsp = function (user) {
        $scope.isp = user;
        $('#myModalIsp').foundation('reveal', 'close');
    }
    //выбираем ответственного по письму
    $scope.setOtvet = function (user) {
        $scope.otvet = user;
        $scope.user_id = user.user_id
        $('#myModalOtvet').foundation('reveal', 'close');
    }
    //выбираем ответственного по письму
    $scope.setOtvetArr = function (user) {
        $scope.otvet = user;
        $scope.user_id = user.user_id;
        $scope.otvets.push(user);

       // $scope.users_id.push(user.user_id);
        $('#myModalOtvet').foundation('reveal', 'close');
    }

    //Удаляем ответсвенного из массива
    $scope.delOtvetArr = function (index) {
        $scope.otvets.splice(index, 1);
    }

    //выбираем чья резолюция
    $scope.setResol = function (user) {
        $scope.resol = user;
        $('#myModalResol').foundation('reveal', 'close');
    }


    //Переход по кнопке Ok
    $scope.goToOutbox = function (doctype) {
        if ($scope.reqRes === 1) {
            if (doctype == 1) {
                $('#myModalResult').foundation('reveal', 'close');
                $location.path('/outbox');
            } else if (doctype == 0) {
                $('#myModalResult').foundation('reveal', 'close');
                $location.path('/inbox');
            }

        }
        else {
            $('#myModalResult').foundation('reveal', 'close');
        }
    }

    //Список отделов
    $scope.getOtdels = function () {
        $scope.currentPage = 0;
        $scope.startItem = 0;
        $scope.limit = 30;
        $scope.numberOfPages = [];
        $scope.loader = true;
        $scope.otdels = $scope.otdels_query.get(function () {
            if ($scope.otdels[0].isAuth == false) {
                $scope.loginmes = true;
                $scope.loginMes = "Вы не имеете доступ к данной странице";
                $location.path('/login')
            } else {
                $scope.count = Math.ceil($scope.otdels.length / $scope.limit);
                for (var i = 0; i < $scope.count; i++) {
                    $scope.numberOfPages.push(i);
                }
                $scope.loader = false;
            }
        });


    }
    //добавляем отдел
    $scope.addOtdel = function () {
        $scope.resp = $scope.otdels_query_add.get({otdel_name: $scope.otdel_name, otdel_code: $scope.otdel_code }, function () {
            if ($scope.resp.otdel_id != null) {
                //var otdeladded = {otdel_id: $scope.resp.otdel_id, otdel_name: $scope.otdel_name, otdel_code: $scope.otdel_code };
                $scope.otdels.push($scope.resp);
            }
        });

        $scope.otdel_name = "";
        $scope.otdel_code = "";
    }
    //редактируем отдел
    $scope.editOtdel = function () {
        $scope.otdel_query_edit.query({otdel_id: $scope.modal_otdel_id, otdel_name: $scope.modal_otdel_name, otdel_code: $scope.modal_otdel_code}, function success() {
                $scope.otdels[$scope.modal_index].otdel_name = $scope.modal_otdel_name;
                $scope.otdels[$scope.modal_index].otdel_code = $scope.modal_otdel_code;
                $('#modalEditOtdel').foundation('reveal', 'close');
            },
            function error() {
                alert("error");
            });

    }
    //удаляем отдел
    $scope.delOtdel = function (id) {
        $scope.otdels_query_del.query({otdel_id: id }, function () {
            $scope.getOtdels();
        });

    }


    $scope.getDolgnesId = function (id) {
        $scope.dolgns = $scope.dolgnes_id_query.get({otdel_id: id});
        $scope.cat = false;
        $templateCache.removeAll();

    }

    $scope.addDolgn = function () {
        var new_dolgn = $scope.dolgn_query_add.get({dolgn_name: $scope.dolgn_name, 'otdel_d.otdel_id': $scope.otdel.otdel_id }, function () {

            $scope.dolgns.push(new_dolgn);
        });
        // var dolgadded = {dolgn_id: $scope.dolgns.length, dolgn_name: $scope.dolgn_name, otdel_d: {otdel_name: $scope.otdel.otdel_name} };
        $scope.dolgn_name = "";

    }

    $scope.delDolgn = function (id) {
        $scope.dolgns_query_del.query({dolgn_id: id }, function () {
            $scope.getDolgnes();
        });

    }

    //удаляем документ
    $scope.delDoc = function (doctype) {
        $scope.docs_query_del.query({docs_id: $routeParams.id }, function () {

            if (doctype == 0) {
                $location.path('/inbox');

            }
            else if (doctype == 1) {
                $location.path('/outbox');
            }
        });

    }


    //Список пользователей
    $scope.getStatuses = function () {
        $scope.statuses = $scope.statuses_query.get();
        $scope.cat1 = false;
        $templateCache.removeAll();

    }

    //Листаем страницы
    $scope.changePage = function (action) {
        $scope.i;
        if (action==="first"){
            $scope.i = 1;
            $scope.start($scope.i*$scope.limit);
        }
        if (action===true){
            if ($scope.i<=$scope.numberOfPages.length-2){
                $scope.i = $scope.i+1;
                $scope.start($scope.i*$scope.limit);
            }
        } else {
            if ($scope.i!=0){
                $scope.i = $scope.i-1;
                $scope.start($scope.i*$scope.limit);
            }
        }


    }


    //Действия после загрузки файла
    $scope.uploadFile = function (content, completed) {
        if (completed) {
            var splitted = content.split("#");
            if (content == "falseFormat") {
                $scope.docNumber = "Прикладываются сканы только в формате PDF"
            } else if (content == "noFile") {
                $scope.docNumber = "Вы не приложили скан письма"
            } else if (content == "noFile") {
                $scope.docNumber = "Вы не приложили скан письма"
            } else if(splitted[0]==="AlreadyReg"){
                $scope.docNumber = "Раннее письмо уже регистрировалось за номером "+splitted[1]
            }
            else {
                $scope.reqRes = 1;
                $scope.docNumber = "Документ успешно создан за номером " + content;
                $scope.otvet = null;
                $scope.isp = null;
                $scope.resol = null;
            }
            $('#myModalResult').foundation('reveal', 'open');


        }

        else
            $scope.docNumber = "[Status: Incomplete] Content ignored. Check log the actual content.";
    }
}


myModule.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }

});
myModule.filter('bydate', function () {
    return function (docs, date) {
        var items = {
            docs_id: docs_id,
            out: []
        };
        for (var i = 0; i < docs.length; i++) {
            if (docs[i].docs_status === "K" && docs[i].docs_date == date) {
                this.out.push($scope.docs[i].docs_id);
            }
        }
        return items.out;
    }
});


