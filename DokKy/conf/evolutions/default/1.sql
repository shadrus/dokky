# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table otdel (
  otdel_id                  integer auto_increment not null,
  otdel_name                varchar(255),
  constraint pk_otdel primary key (otdel_id))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table otdel;

SET FOREIGN_KEY_CHECKS=1;

