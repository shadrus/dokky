import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "DokKy"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    javaCore,
    javaJdbc,
    javaEbean,
    "mysql" % "mysql-connector-java" % "5.1.22",
    "org.hibernate" % "hibernate-entitymanager" % "4.2.0.Final",
    "org.imgscalr"%"imgscalr-lib" % "4.2",
    "org.apache.commons"%"commons-email"%"1.3.1",
    "org.apache.poi"%"poi"%"3.9",
    "org.codehaus.jackson" % "jackson-core-asl" % "1.9.13"
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here      
  )

}
